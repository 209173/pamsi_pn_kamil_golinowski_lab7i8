#include <windows.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <conio.h>
#include <cmath>


using namespace std;

struct Wierzcholek{
	public:
	int numer;
	
	int Ktory(){
		return this->numer;
	}
	
	void Ktory(int i){
		this->numer=i;
	}		
	
};

class Krawedz{
	public:
	int waga;
	Wierzcholek *poczatek, *koniec;
	Krawedz *next;
	
	int Waga(){
		return this->waga;
	}
	
	void Waga(int i){
		this->waga=i;
	}
	
	int Pierwszy(){
		return this->poczatek->Ktory();
	}
	
	int Ostatni(){
		return this->koniec->Ktory();
	}
	
	
};

class graf
{
public:
	
	int tabPom[10000];
	int wymiar; 					//ilosc wierzcholkow
	int rozmiar;					//ilosc krawedzi
	Krawedz *tab_krawedzi;			//tablica krawedzi
	Wierzcholek *tab_wierzcholkow;	//tablica wierzcholkow, o 1 wieksza niz rzeczywisty rozmiar, miejsce pierwsze-puste
	Krawedz *min_drzewo;			//tablica minimalnego drzewa rozpinajacego dany graf- dlugosc: wymiar-1
	int rozpietosc;					//zmienna pomocnicza do zapisania rozpietosci drzewa
	
	void UstalWierzcholki(int wymiar){
		tab_wierzcholkow=new Wierzcholek [wymiar+1];
		for(int i=1; i<wymiar+1; i++)
			this->tab_wierzcholkow[i].numer=i;
	}
	
	void LosujKrawedz(int i){
		int a=rand() % wymiar + 1;
		int b=rand() % wymiar + 1;

		this->tab_krawedzi[i].Waga(rand()%999);
		this->tab_krawedzi[i].poczatek=&tab_wierzcholkow[a];
		this->tab_krawedzi[i].koniec=&tab_wierzcholkow[b];
		if(a==b)
			LosujKrawedz(i);
	}
	
	void losujGraf()
	{
		int gestosc;
		double procent;
		int maxIloscKrawedzi;
		
		cout << "Podaj ilosc wierzcholkow: "; 
		cin >> this->wymiar; 
		cout << endl;
		cout << "1. 25%" << endl;
		cout << "2. 50%" << endl;
		cout << "3. 75%" << endl;
		cout << "4. 100%" << endl;
		cout << "Wybierz gestosc grafu: ";
		cin >> gestosc; cout << endl;

		switch(gestosc){
		case 1:
			procent=0.25; break;
		case 2:
			procent=0.5; break;
		case 3:
			procent=0.75; break;
		case 4:
			procent=1; break;
		default:
			break;
		}

		maxIloscKrawedzi = (this->wymiar*(this->wymiar-1))/2;		//ustalenie maxymalnej ilosci krawedzi
		this->rozmiar=maxIloscKrawedzi*procent;				//ustalenie ilosci krawedzi dla danego procentu
		tab_krawedzi = new Krawedz [rozmiar];			//alokacja tablicy krawedzi
		this->UstalWierzcholki(wymiar);						//ustalenie wierzcholkow
		for(int i=0; i<rozmiar; i++)				//wylosowanie krawedzi
			LosujKrawedz(i);
	}

	void wczytajZPliku(string nazwaPliku)
	{
		rozmiar = 0;

		ifstream plik;
		plik.open(nazwaPliku.c_str());

		for(int i=0; !plik.eof(); i++){
			rozmiar++;
			plik >> tabPom[i];}
		rozmiar= rozmiar/3;

		plik.clear();
		plik.seekg(0, ios::beg);

		tab_krawedzi = new Krawedz[rozmiar];
		for(int j = 0; j<rozmiar; j++){
				plik  >> tab_krawedzi[j].poczatek->numer >> tab_krawedzi[j].koniec->numer >> tab_krawedzi[j].waga;
		}
		plik.close();
		
		wymiar=(1+sqrt(1+8*rozmiar))/2;
		UstalWierzcholki(wymiar);
		
	}

	void zapiszWynik(string nazwaPliku)
	{
		ofstream plik;
		plik.open(nazwaPliku.c_str());
		
		for(int j=0; j<wymiar-1; j++){
			
		plik << min_drzewo[j].poczatek->Ktory()<<" "<<min_drzewo[j].koniec->Ktory() << " "<<min_drzewo[j].Waga();
		plik << endl;
		}
		plik.close();
}

	
	void Quicksort(int lewy,int prawy){
	int i=lewy,j=prawy;
	Krawedz pom;						// do przestawiania elem
	int pivot=this->tab_krawedzi[(lewy+prawy)/2].Waga();	// piwot elem srodkowy
	while(i<=j){
		while(tab_krawedzi[i].Waga()<pivot)		// nie ruszamy elem dopoki nie wiekszy
			i++;
		while(tab_krawedzi[j].Waga()>pivot)		// nie ruszamy poki nie mniejszy
			j--;
		if(i<=j){						// zamieniamy by byly odpowiednio
			pom=tab_krawedzi[i];
			tab_krawedzi[i]=tab_krawedzi[j];
			tab_krawedzi[j]=pom;
			i++;
			j--;
		}
	}
	if(lewy<j)
		Quicksort(lewy,j);
	if(i<prawy)
		Quicksort(i,prawy);
}

	int Znajdz(int i, int j){
		for(int u=0; u<rozmiar; u++)
			if(i!=tab_krawedzi[u].Pierwszy()&&j!=tab_krawedzi[u].Ostatni())
				return u;
	}
	
	
	
	void Sprawdz_cykl(int C[], int u, int v, int &l, int x, int &d){
		
		if (C[u]!=0&&C[v]==0){	// dodanie wierzcholka do drzewa
  			min_drzewo[l].poczatek=tab_krawedzi[x].poczatek;
			min_drzewo[l].koniec=tab_krawedzi[x].koniec;
			min_drzewo[l].waga=tab_krawedzi[x].waga;
			C[v]=C[u];
			l++;
			this->rozpietosc++;
		}
	
	   if (C[u]==0&&C[v]!=0){	// dodanie wierzcholka do drzewa
			min_drzewo[l].poczatek=tab_krawedzi[x].poczatek;
			min_drzewo[l].koniec=tab_krawedzi[x].koniec;
			min_drzewo[l].waga=tab_krawedzi[x].waga;
			C[u]=C[v];
			l++;
			this->rozpietosc++;
	   }

	   if (C[u]==0&&C[v]==0){	// tworzenie nowego drzewa
			min_drzewo[l].poczatek=tab_krawedzi[x].poczatek;
			min_drzewo[l].koniec=tab_krawedzi[x].koniec;
			min_drzewo[l].waga=tab_krawedzi[x].waga;
			C[v]=d;
			C[u]=d;
			l++;
			d++;
			this->rozpietosc++;
	   }
	}
	
	int Znajdz_K(int pocz, int kon){
		for(int i=0; i<rozmiar; i++){
			if(tab_krawedzi[i].Pierwszy()==pocz && tab_krawedzi[i].Ostatni()==kon){
				return i;
				break;
			}
		}
	}
	
};

void Qsort(Krawedz tab_krawedzi[], int lewy,int prawy){
	int i=lewy,j=prawy;
	Krawedz pom;						// do przestawiania elem
	int pivot=tab_krawedzi[(lewy+prawy)/2].Waga();	// piwot elem srodkowy
	while(i<=j){
		while(tab_krawedzi[i].Waga()<pivot)		// nie ruszamy elem dopoki nie wiekszy
			i++;
		while(tab_krawedzi[j].Waga()>pivot)		// nie ruszamy poki nie mniejszy
			j--;
		if(i<=j){						// zamieniamy by byly odpowiednio
			pom=tab_krawedzi[i];
			tab_krawedzi[i]=tab_krawedzi[j];
			tab_krawedzi[j]=pom;
			i++;
			j--;
		}
	}
	if(lewy<j)
		Qsort(tab_krawedzi, lewy,j);
	if(i<prawy)

		Qsort(tab_krawedzi, i,prawy);
}

class Macierz:public graf{
	public:
		
	int **tabM;      //zmienna macierzy sasiectwa
	
	void utworzMacierzSasiedztwa()
	{
		tabM = new int*[wymiar];		//zaalokowanie macierzy sasiedztwa
		for (int i = 0; i < wymiar; i++)
			tabM[i] = new int[wymiar];

		for(int i=0; i<rozmiar; i++){								//wypelnienie jedynkami elementow polaczonych
			tabM[tab_krawedzi[i].Pierwszy()-1][tab_krawedzi[i].Ostatni()-1] = 1;
			tabM[tab_krawedzi[i].Ostatni()-1][tab_krawedzi[i].Pierwszy()-1] = 1;
		}
		for(int i=0; i<wymiar; i++){								//wypelnienie zerami elementow nie polaczonych
			for(int j=0;j<wymiar;j++)
				if(tabM[i][j]!=1)
					tabM[i][j]=0;
		}
	}

	void Wyswietl(){
		for(int i=0;i<wymiar;i++){
			for(int j=0;j<wymiar;j++){
				cout<<tabM[i][j]<<" ";
			}
			cout<<endl;
		}
	}

int Czy_Incyd(Krawedz Z[],int C[]){
	int x=0;

	for(int i=0;i<wymiar;i++)
		if(C[i]==1){					// dla dalaczonych wierzcholkw
			for(int j=0;j<wymiar;j++)
				if(tabM[i][j]!=0){	// wypisujemy wszystkie niedolaczone
					Z[x].poczatek->Ktory(i);
					Z[x].koniec->Ktory(j);
					Z[x].waga=tab_krawedzi[Znajdz_K(i,j)].Waga();
					x++;
				}
		}
		
	return x;
}

		void kruskalMacierz()
	{
		min_drzewo = new Krawedz[wymiar-1];
		this->rozpietosc=0;
		int x=0;									//zmienna pomocnicza do przechodzenia po wszystkich krawedziach
		int l=0;									//zmienna pomocnicza do przechodzenia po minimalnym drzewie
		int d=1;									//zmienna pomocnicza pokazujaca ilosc stworzonych drzew w trakcie algorytmu
		int u=0,v=0;
		
		this->Quicksort(0,this->rozmiar);		//sortowanie wg wag
		cout<<this->tab_krawedzi[0].Pierwszy()<<endl;
		int *C;
		C=new int[wymiar];              //tablica przechodzenia przez wierzcholki
		for(int i=0;i<wymiar;i++)
			C[i]=0;
	while(x<rozmiar){				// tak dlugo jak niesprawdzone krawedzie		
        u=this->tab_krawedzi[x].poczatek->numer;
        v=this->tab_krawedzi[x].koniec->numer;
        
		if (C[u]!=0&&C[v]==0){	// dodanie wierzcholka do drzewa
  			min_drzewo[l].poczatek=tab_krawedzi[x].poczatek;
			min_drzewo[l].koniec=tab_krawedzi[x].koniec;
			min_drzewo[l].waga=tab_krawedzi[x].waga;
			C[v]=C[u];
			l++;
			this->rozpietosc++;
		}
	
	   if (C[u]==0&&C[v]!=0){	// dodanie wierzcholka do drzewa
			min_drzewo[l].poczatek=tab_krawedzi[x].poczatek;
			min_drzewo[l].koniec=tab_krawedzi[x].koniec;
			min_drzewo[l].waga=tab_krawedzi[x].waga;
			C[u]=C[v];
			l++;
			this->rozpietosc++;
	   }

	   if (C[u]==0&&C[v]==0){	// tworzenie nowego drzewa
			min_drzewo[l].poczatek=tab_krawedzi[x].poczatek;
			min_drzewo[l].koniec=tab_krawedzi[x].koniec;
			min_drzewo[l].waga=tab_krawedzi[x].waga;
			C[v]=d;
			C[u]=d;
			l++;
			d++;
			this->rozpietosc++;
	   }
	x++;										// nastepna krawedz
	}
	
	
	}

	void primMacierz()
	{
		Krawedz *C;
		min_drzewo = new Krawedz[wymiar-1];
		this->rozpietosc=0;
		int *odw = new int[wymiar];
		int U,V;
		int n;
		
		this->Quicksort(0,this->rozmiar);

		for (int i = 0; i < wymiar; i++){
			odw[i] = 1;
		}
		C=new Krawedz[rozmiar];
		
		U=tab_krawedzi[0].Pierwszy();
		V=tab_krawedzi[0].Ostatni();
		
		odw[0]=1;
		
		min_drzewo[rozpietosc]=tab_krawedzi[rozpietosc];
		rozpietosc++;
		n=Czy_Incyd(C,odw);
		while(n>0){
			Qsort(C,0, n-1);
			min_drzewo[rozpietosc].poczatek=C[rozpietosc].poczatek;
			min_drzewo[rozpietosc].koniec=C[rozpietosc].koniec;
			min_drzewo[rozpietosc].Waga(C[rozpietosc].Waga());
			odw[rozpietosc]=1;
			rozpietosc++;
			n=Czy_Incyd(C,odw);
		}
		
		
	}
	

};



class Lista:public graf{
	public:
		
	Krawedz **listS;    /////
	
	// Dodaje krawedz do listy sasiadow
	void Dodaj_Do_ls(int w_p,int w_k,int waga){

	//w_p - wierzcholek poczatkowy
	//w_k - wierzcholek koncowy

	Krawedz *pom1,*pom2;       //zmienne pomocnicze

	pom1=listS[w_p];
	if(pom1==NULL){				// jesli nie bylo wczesniej elementow
		pom2=new Krawedz;		// o danym wezle poczatkowym
		pom2->next=NULL;		// dodajemy krawedz jako pierwsza
		pom2->poczatek->Ktory(w_p);
		pom2->koniec->Ktory(w_k);
		pom2->waga=waga;
		listS[w_p]=pom2;
	}
	else{						// jesli byly wczesniej elementy
		while(pom1->next!=NULL)	// o danym wezle poczatkowym
			pom1=pom1->next;	// idziemy na koniec
		pom2=new Krawedz;		// dodajemy krawedz
		pom2->next=NULL;
		pom2->poczatek->Ktory(w_p);
		pom2->koniec->Ktory(w_k);
		pom2->waga=waga;
		pom1->next=pom2;
		pom2=NULL;
	}

}

	// Tworzy liste sasiadow
	void Stworz_ls(){
		listS=new Krawedz* [rozmiar];
	for(int i=0;i<rozmiar;i++)
		for(int j=0;j<rozmiar;j++)
			Dodaj_Do_ls(tab_krawedzi[i].Pierwszy(),tab_krawedzi[i].Ostatni(),tab_krawedzi[i].Waga()); //dodajac krawedzie tworzymy liste sasiadow
	}
	
	
	// Sprawdza czy istnieje nastepujaca krawedz dla listy sasiedztwa, zwraca jej wage
int Czy_Kraw_ls(int n,int m){
	Krawedz* pom;

	if(n>=wymiar)
		return -1;					// nie ma takiego wezla
	pom=listS[n];
	if(pom==NULL)
		return 0;					// nie ma polaczenia
	do{
		if(pom->Ostatni()==m)
			return pom->waga;		// zwracamy wage
		pom=pom->next;
	}while(pom!=NULL);

	return 0;
}


// Tworzy tablice mozliwych krawedzi listy sasiadow
int Tab_Kraw_Pri_ls(Krawedz *Z,int *C){
	Krawedz *pom;
	int x=0;

	for(int i=0;i<wymiar;i++)
		if(C[i]==1){					// dla dalaczonych wierzcholkw
			for(pom=listS[i] ; pom!=NULL ; pom=pom->next)
				if(C[pom->Ostatni()]!=1){	// wypisujemy wszystkie niedolaczone
					Z[x].poczatek=pom->poczatek;
					Z[x].koniec=pom->koniec;
					Z[x].waga=pom->waga;
					x++;
				}
			for(int j=i-1;j>=0;j--)
				for(pom=listS[j] ; pom!=NULL ; pom=pom->next)
					if(pom->Ostatni()==i && C[pom->Pierwszy()]!=1){
						Z[x].poczatek=pom->koniec;
						Z[x].koniec=pom->poczatek;
						Z[x].waga=pom->waga;
						x++;
					}
		}
	pom=NULL;
	return x;
}

// Algorytm Kruskala dla listy sasiadow
void kruskalLista(){
	int *C;					
	int u=0,v=0;				// (u,v)-krawedz
	int x=0;
	int mst=0;				// mst
	int d=1;				// numer drzewa
	int q=0;
	int min=9999;


	C=new int[wymiar];			// alokujemy tyle ile jest wezlow
	for(int i=0;i<wymiar;i++)
		C[i]=0;

	while(x<rozmiar){				// tak dlugo jak niesprawdzone krawedzie
		for (int i=0;i<wymiar;i++){  //znajduje minimalna wartosc w liscie krawedzi
			if((listS[i]->waga)<min&& (listS[i]->waga)>0){
				min=listS[i]->waga;
				q=i;
			}
		}

        u=listS[q]->Pierwszy();
        v=listS[q]->Ostatni();

        if (C[u]!=0&&C[v]==0){	// dodanie wierzcholka do drzewa
            C[v]=C[u];
		}
	   if (C[u]==0&&C[v]!=0){	// dodanie wierzcholka do drzewa
            C[u]=C[v];
	   }
	   if (C[u]==0&&C[v]==0){	// tworzenie nowego drzewa
            C[v]=d;
			C[u]=d;
			d++;
	   }
	listS[q]->waga=10000;
	min=9999;
	x++;						// nastepna krawedz
	}
}

// Algorytm Prima dla listy sasiadow
int primLista(){
	int *C;						// tab oznaczajaca wierzcholki juz w drzewie
	Krawedz *Z;					// tab krawedzi do posortowania
	int mst=0;					// mst
	int n;						// ilosc krawedzi do posortwoania

	Z=new Krawedz[rozmiar];			// alokujemy tyle ile jest krawedzi
	C=new int[wymiar];				// alokujemy tyle ile jest wezlow
	for(int i=0;i<wymiar;i++)
		C[i]=0;
	C[0]=1;						// zaczynamy od wezla 0

	n=Tab_Kraw_Pri_ls(Z,C);		// tworzymy tablice krawedzi
	while(n>0){					// jesli jest jeszce krawedz do dodania
		Qsort(Z,0,n-1);		// sortujemy krawedzie od najmniejszej wagi
		C[Z[0].Ostatni()]=1;
		n=Tab_Kraw_Pri_ls(Z,C);	// dodajemy mozliwe krawedzie
	}
	
}

	
};

int main()
{
	Macierz a;
	Lista b;
	int wybor,wybor1,wybor2;
	string nazwa;

	cout << "Wybierz opcje: " << endl;
	cout << "1. Wczytaj graf z pliku." << endl;
	cout << "2. Generuj losowy graf." << endl;
	cout << "Twoj wybor: "; cin >> wybor; cout<< endl;
	if(wybor == 1){
	do{
		cout << "Wybierz algorytm:" << endl;
		cout << "1. Prim." << endl;
		cout << "2. Kruskal." << endl;
		cout << "0. Zakoncz program." << endl;
		cout << "Twoj wybor: "; cin >> wybor1; cout<<endl;
		switch(wybor1){
		case 1:
			cout << "Wybierz reprezentacje:" << endl;
			cout << "1. Macierz Sasiedztwa. " << endl;
			cout << "2. Lista Sasiadow." << endl;
			cout << "Twoj wybor: "; cin>>wybor2; cout << endl;
			switch(wybor2){
			case 1:
				cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				a.wczytajZPliku(nazwa.c_str());
				a.UstalWierzcholki(a.wymiar);
				a.utworzMacierzSasiedztwa();
				a.primMacierz();
				cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane(z rozszerzeniem .txt): ";
				cin>>nazwa;
				a.zapiszWynik(nazwa.c_str());
				break;
			case 2:
				cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				b.wczytajZPliku(nazwa.c_str());
				b.UstalWierzcholki(b.wymiar);
				b.Stworz_ls();
				b.primLista();
				cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				b.zapiszWynik(nazwa.c_str());
				break;
			default:
				break;
			}
			break;
		case 2:
			cout << "Wybierz reprezentacje:" << endl;
			cout << "1. Macierz Sasiedztwa. " << endl;
			cout << "2. Lista Sasiadow." << endl;
			cout << "Twoj wybor: "; cin>>wybor2; cout << endl;
			switch(wybor2){
			case 1:
				cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				a.wczytajZPliku(nazwa.c_str());
				a.UstalWierzcholki(a.wymiar);
				a.utworzMacierzSasiedztwa();
				a.kruskalMacierz();
				cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				a.zapiszWynik(nazwa.c_str());
				break;
			case 2:
				cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				b.wczytajZPliku(nazwa.c_str());
				b.UstalWierzcholki(b.wymiar);
				b.Stworz_ls();
				b.kruskalLista();
				cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				b.zapiszWynik(nazwa.c_str());
				break;
			default:
				break;
			}
			break;
		case 0:
			break;
		}
	}while(wybor1!=0);
	}
	if (wybor ==2){

	a.losujGraf();
	a.utworzMacierzSasiedztwa();
	a.kruskalMacierz();
	cout<<"Podaj nazwe pliku do ktorego chcesz zapisac dane (z rozszerzeniem .txt): ";
	cin>>nazwa;
	a.zapiszWynik(nazwa.c_str());
	}
}
